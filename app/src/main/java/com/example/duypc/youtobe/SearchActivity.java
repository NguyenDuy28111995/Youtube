package com.example.duypc.youtobe;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;


import com.example.duypc.youtobe.adapter.NewVideoAdapter;
import com.example.duypc.youtobe.object.NewVideo;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Admin on 06/08/16.
 */
public class SearchActivity extends Activity {
    private EditText et_search;
    private ImageView iv_btn_back;
    private ListView lv_playlist_category;
    private ArrayList<NewVideo> newVideos, list;
    private NewVideoAdapter newVideoAdapter;
    //=============begin=======
    private static char[] SPECIAL_CHARACTERS = {'À', 'Á', 'Â', 'Ã', 'È', 'É',
            'Ê', 'Ì', 'Í', 'Ò', 'Ó', 'Ô', 'Õ', 'Ù', 'Ú', 'Ý', 'à', 'á', 'â',
            'ã', 'è', 'é', 'ê', 'ì', 'í', 'ò', 'ó', 'ô', 'õ', 'ù', 'ú', 'ý',
            'Ă', 'ă', 'Đ', 'đ', 'Ĩ', 'ĩ', 'Ũ', 'ũ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ạ',
            'ạ', 'Ả', 'ả', 'Ấ', 'ấ', 'Ầ', 'ầ', 'Ẩ', 'ẩ', 'Ẫ', 'ẫ', 'Ậ', 'ậ',
            'Ắ', 'ắ', 'Ằ', 'ằ', 'Ẳ', 'ẳ', 'Ẵ', 'ẵ', 'Ặ', 'ặ', 'Ẹ', 'ẹ', 'Ẻ',
            'ẻ', 'Ẽ', 'ẽ', 'Ế', 'ế', 'Ề', 'ề', 'Ể', 'ể', 'Ễ', 'ễ', 'Ệ', 'ệ',
            'Ỉ', 'ỉ', 'Ị', 'ị', 'Ọ', 'ọ', 'Ỏ', 'ỏ', 'Ố', 'ố', 'Ồ', 'ồ', 'Ổ',
            'ổ', 'Ỗ', 'ỗ', 'Ộ', 'ộ', 'Ớ', 'ớ', 'Ờ', 'ờ', 'Ở', 'ở', 'Ỡ', 'ỡ',
            'Ợ', 'ợ', 'Ụ', 'ụ', 'Ủ', 'ủ', 'Ứ', 'ứ', 'Ừ', 'ừ', 'Ử', 'ử', 'Ữ',
            'ữ', 'Ự', 'ự',};

    private static char[] REPLACEMENTS = {'A', 'A', 'A', 'A', 'E',
            'E', 'E', 'I', 'I', 'O', 'O', 'O', 'O', 'U', 'U', 'Y', 'a', 'a',
            'a', 'a', 'e', 'e', 'e', 'i', 'i', 'o', 'o', 'o', 'o', 'u', 'u',
            'y', 'A', 'a', 'D', 'd', 'I', 'i', 'U', 'u', 'O', 'o', 'U', 'u',
            'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A',
            'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'A', 'a', 'E', 'e',
            'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E',
            'e', 'I', 'i', 'I', 'i', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o',
            'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O', 'o', 'O',
            'o', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u',
            'U', 'u', 'U', 'u',};

    public static char VToE(char ch) {
        int index = Arrays.binarySearch(SPECIAL_CHARACTERS, ch);
        if (index >= 0) {
            ch = REPLACEMENTS[index];
        }
        return ch;
    }

    public String VToEs(String s) {
        String result = "";
        for (int i = 0; i < s.length(); i++) {
            result += VToE(s.charAt(i));
        }
        return result;
    }

    //============end===========

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        et_search = (EditText) findViewById(R.id.et_search);
        iv_btn_back = (ImageView) findViewById(R.id.iv_btn_back);
        lv_playlist_category = (ListView) findViewById(R.id.lv_playlist_category);
        iv_btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        newVideos = (ArrayList<NewVideo>) getIntent().getSerializableExtra("newvideos");
        list = newVideos;
        newVideoAdapter = new NewVideoAdapter(list, this);
        lv_playlist_category.setAdapter(newVideoAdapter);
        et_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                String s1, s2;
                s2 = VToEs(s.toString());
                list = new ArrayList<>();
                if (s.length() == 0) {
                    list = newVideos;
                } else {

                    for (int i = 0; i < newVideos.size(); i++) {
                        s1 = VToEs(newVideos.get(i).getTitle().toLowerCase());
                        if (s1.indexOf(s2) >= 0) {
                            list.add(newVideos.get(i));
                        }
                    }
                }
                newVideoAdapter = new NewVideoAdapter(list, SearchActivity.this);
                lv_playlist_category.setAdapter(newVideoAdapter);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });
        lv_playlist_category.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(SearchActivity.this, VideoYoutobe.class);
                intent.putExtra(VideoYoutobe.VIDEO_ID, newVideos.get(position).getVideoId());
                intent.putExtra(VideoYoutobe.TITLE, newVideos.get(position).getTitle());
                intent.putExtra(VideoYoutobe.VIEWCOUNT, newVideos.get(position).getViewCount());
                intent.putExtra(VideoYoutobe.URL, newVideos.get(position).getUrl());
                intent.putExtra(VideoYoutobe.PLAYLISTID, newVideos.get(position).getPlaylistId());
                intent.putExtra(VideoYoutobe.TIME, newVideos.get(position).getDuration());
                intent.putExtra(VideoYoutobe.ACTION, "1");
                startActivity(intent);
            }
        });
    }
}