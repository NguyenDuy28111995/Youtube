package com.example.duypc.youtobe.object;

import java.io.Serializable;

/**
 * Created by Admin on 06/06/16.
 */
public class Account implements Serializable {
    private String id,name,gmail,url;

    public Account() {
    }

    public Account(String id,String gmail, String name,  String url) {
        this.name = name;
        this.gmail = gmail;
        this.id = id;
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGmail() {
        return gmail;
    }

    public void setGmail(String gmail) {
        this.gmail = gmail;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
