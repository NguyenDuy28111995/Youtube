package com.example.duypc.youtobe.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.duypc.youtobe.CatelogyActivity;
import com.example.duypc.youtobe.R;
import com.example.duypc.youtobe.object.Playlist;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by DuyPC on 6/7/2016.
 */
public class Category_adapter extends BaseAdapter {
    private ArrayList<Playlist> list;
    private Activity activity;
    public Category_adapter(ArrayList<Playlist> list, Activity activity) {
        this.list = list;
        this.activity = activity;

    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if(v == null){
            v = LayoutInflater.from(activity).inflate(R.layout.item_category, null);
        }
        final Playlist item =  list.get(position);
        ImageView iv_image = (ImageView)v.findViewById(R.id.iv_image);
        TextView tv_title = (TextView) v.findViewById(R.id.tv_title);
        tv_title.setText(item.getTitle());
        String url = item.getThumbUrl();
        Picasso.with(activity)
                .load(url)
                .resize(250,250)
                .centerCrop()
                .into(iv_image);
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(activity, CatelogyActivity.class);
                intent.putExtra("playlist",item);
                activity.startActivity(intent);
            }
        });
        return v;
    }
}
